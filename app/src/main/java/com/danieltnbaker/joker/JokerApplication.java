package com.danieltnbaker.joker;

import android.app.Application;

import com.danieltnbaker.joker.di.DI;
import com.danieltnbaker.joker.di.DaggerAppComponent;


public class JokerApplication extends Application {

    private static JokerApplication sSelf;

    public JokerApplication() {
        sSelf = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DI.setAppComponent(DaggerAppComponent.builder().build());
    }

    public static JokerApplication get() {
        return sSelf;
    }
}
