package com.danieltnbaker.joker.textinput.ui;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.danieltnbaker.joker.R;
import com.danieltnbaker.joker.di.DI;
import com.danieltnbaker.joker.model.Joke;
import com.danieltnbaker.joker.textinput.mvp.TextInputMvp;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class TextInputActivity extends AppCompatActivity implements TextInputMvp.View {

    @BindView(R.id.character_text_input)
    EditText characterInput;
    @BindView(R.id.character_text_input_layout)
    TextInputLayout characterInputLayout;

    @Inject TextInputMvp.Presenter mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getBoolean(R.bool.orientation_portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_text_input);
        ButterKnife.bind(this);
        DI.getAppComponent().inject(this);
        mPresenter.setView(this);
    }

    @Override
    protected void onDestroy() {
        mPresenter.releaseView();
        super.onDestroy();
    }

    @OnEditorAction(R.id.character_text_input)
    public boolean onCharacterTextInputChanged(TextView v, int actionId) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            mPresenter.onCharacterSet(v.getText().toString());
            return true;
        }
        return false;
    }

    @OnClick(R.id.button_search)
    public void onCharacterSearchClicked() {
        mPresenter.onCharacterSet(characterInput.getText().toString());
    }

    @Override
    public void showJoke(Joke joke) {
        new AlertDialog.Builder(this)
                .setMessage(Html.fromHtml(joke.getJoke()))
                .setPositiveButton(R.string.dismiss, (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    @Override
    public void showValidationError(@StringRes int resId) {
        characterInputLayout.setError(getString(resId));
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, R.string.error_server_error, Toast.LENGTH_SHORT).show();
    }
}
