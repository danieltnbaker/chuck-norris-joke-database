package com.danieltnbaker.joker.utils;


import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {

    private static final String SHARED_PREFERENCES_POSTFIX = "_preferences";
    private static final String PREF_ALLOW_EXPLICIT_JOKES = "pref_allow_explicit_jokes";
    private final SharedPreferences mPreferences;

    public PreferenceHelper(Context context) {
        mPreferences = context.getSharedPreferences(
                context.getPackageName() + SHARED_PREFERENCES_POSTFIX,
                Context.MODE_PRIVATE);
    }

    public void setAllowExplicitJokesPreference(boolean allow) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putBoolean(PREF_ALLOW_EXPLICIT_JOKES, allow);
        editor.apply();
    }

    public boolean getAllowExplicitJokesPreference() {
        return mPreferences.getBoolean(PREF_ALLOW_EXPLICIT_JOKES, true);
    }

}
