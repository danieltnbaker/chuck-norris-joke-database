package com.danieltnbaker.joker.list.ui;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danieltnbaker.joker.R;
import com.danieltnbaker.joker.model.Joke;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private final List<Joke> mData;

    ListAdapter(List<Joke> data) {
        mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.simple_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    void addData(List<Joke> data) {
        int prev = getItemCount();
        mData.addAll(data);
        notifyItemRangeChanged(prev, data.size());
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.joke_text) TextView textView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Joke joke) {
            textView.setText(Html.fromHtml(joke.getJoke()));
        }
    }
}
