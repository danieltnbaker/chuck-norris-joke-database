package com.danieltnbaker.joker.di;

import com.danieltnbaker.joker.list.di.ListModule;
import com.danieltnbaker.joker.list.ui.ListActivity;
import com.danieltnbaker.joker.menu.di.MenuModule;
import com.danieltnbaker.joker.menu.ui.MenuActivity;
import com.danieltnbaker.joker.textinput.di.TextInputModule;
import com.danieltnbaker.joker.textinput.ui.TextInputActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        NetworkModule.class,
        MenuModule.class,
        ListModule.class,
        TextInputModule.class
})
public interface AppComponent {

    void inject(MenuActivity activity);
    void inject(ListActivity activity);
    void inject(TextInputActivity activity);
}
