package com.danieltnbaker.joker.list.mvp;


import com.danieltnbaker.joker.model.Joke;

import java.util.List;

public interface ListMvp {

    interface Presenter {

        void setView(final ListMvp.View view);
        void onViewReady();
        void onEndOfPageReached();
        void releaseView();
    }

    interface View {
        void loadJokes(final List<Joke> jokes);
        void showProgress();
        void hideProgress();
    }
}
