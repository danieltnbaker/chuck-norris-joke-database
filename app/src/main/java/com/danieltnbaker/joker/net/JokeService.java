package com.danieltnbaker.joker.net;


import com.danieltnbaker.joker.model.JokeMultipleResponse;
import com.danieltnbaker.joker.model.JokeSingleResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface JokeService {

    @GET("jokes/random/20")
    Single<JokeMultipleResponse> getAllJokes(@Query("exclude") String exclude);

    @GET("jokes/random/")
    Single<JokeSingleResponse> getJokeWithCharacter(@Query("firstName") String firstName,
                                                    @Query("lastName") String lastName,
                                                    @Query("exclude") String exclude);

    @GET("jokes/random/")
    Single<JokeSingleResponse> getRandomJoke(@Query("exclude") String exclude);
}
