package com.danieltnbaker.joker.di;

import com.danieltnbaker.joker.model.AutoValueGsonFactory;
import com.danieltnbaker.joker.net.JokeRepo;
import com.danieltnbaker.joker.net.JokeService;
import com.danieltnbaker.joker.utils.PreferenceHelper;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class NetworkModule {

    @Provides
    @Singleton
    JokeService providesJokeService() {
        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(
                new GsonBuilder().registerTypeAdapterFactory(AutoValueGsonFactory.create())
                        .create());
        return new Retrofit.Builder()
                .baseUrl("http://api.icndb.com/")
                .client(new OkHttpClient.Builder().build())
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(JokeService.class);
    }

    @Provides
    @Singleton
    JokeRepo providesMenuRepo(final JokeService service,
                              final PreferenceHelper preferenceHelper) {
        return new JokeRepo(service, preferenceHelper);
    }
}
