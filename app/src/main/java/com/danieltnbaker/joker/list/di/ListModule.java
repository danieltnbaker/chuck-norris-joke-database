package com.danieltnbaker.joker.list.di;

import com.danieltnbaker.joker.list.mvp.ListMvp;
import com.danieltnbaker.joker.list.mvp.ListPresenter;
import com.danieltnbaker.joker.net.JokeRepo;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


@Module
public class ListModule {

    @Provides
    ListMvp.Presenter provideListPresenter(JokeRepo repo) {
        return new ListPresenter(repo, Schedulers.io(), AndroidSchedulers.mainThread());
    }
}
