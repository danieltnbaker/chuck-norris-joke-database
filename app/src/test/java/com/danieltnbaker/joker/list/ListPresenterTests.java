package com.danieltnbaker.joker.list;


import com.danieltnbaker.joker.list.mvp.ListMvp;
import com.danieltnbaker.joker.list.mvp.ListPresenter;
import com.danieltnbaker.joker.model.Joke;
import com.danieltnbaker.joker.net.JokeRepo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.TestScheduler;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ListPresenterTests {

    @Mock
    private ListMvp.View view;
    @Mock
    private JokeRepo repo;

    private ListMvp.Presenter presenter;
    private TestScheduler testScheduler;

    @Before
    public void setup() {
        testScheduler = new TestScheduler();
        presenter = new ListPresenter(repo, testScheduler, testScheduler);
        presenter.setView(view);
    }

    @After
    public void tearDown() {
        presenter.releaseView();
    }

    @Test
    public void testLoadingOfJokesOnViewReady() {
        List<Joke> listOfJokes = getListOfJokes();
        Single<List<Joke>> single = Single.just(listOfJokes);
        when(repo.getAllJokes()).thenReturn(single);
        presenter.onViewReady();
        verify(view).showProgress();
        verify(repo).getAllJokes();
        testScheduler.triggerActions();
        verify(view).loadJokes(eq(listOfJokes));
        verify(view).hideProgress();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testHandlesNetworkErrorOnViewReady() {
        Single<List<Joke>> single = Single.error(new Throwable());
        when(repo.getAllJokes()).thenReturn(single);
        presenter.onViewReady();
        verify(view).showProgress();
        verify(repo).getAllJokes();
        testScheduler.triggerActions();
        verify(view).hideProgress();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testNoInteractionsWhenViewReleasedOnViewReady() {
        List<Joke> listOfJokes = getListOfJokes();
        Single<List<Joke>> single = Single.just(listOfJokes);
        when(repo.getAllJokes()).thenReturn(single);
        presenter.onViewReady();
        verify(view).showProgress();
        verify(repo).getAllJokes();
        presenter.releaseView();
        testScheduler.triggerActions();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testLoadingOfJokesOnEndOfPageReached() {
        List<Joke> listOfJokes = getListOfJokes();
        Single<List<Joke>> single = Single.just(listOfJokes);
        when(repo.getAllJokes()).thenReturn(single);
        presenter.onEndOfPageReached();
        verify(view).showProgress();
        verify(repo).getAllJokes();
        testScheduler.triggerActions();
        verify(view).loadJokes(eq(listOfJokes));
        verify(view).hideProgress();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testHandlesNetworkErrorOnEndOfPageReached() {
        Single<List<Joke>> single = Single.error(new Throwable());
        when(repo.getAllJokes()).thenReturn(single);
        presenter.onEndOfPageReached();
        verify(view).showProgress();
        verify(repo).getAllJokes();
        testScheduler.triggerActions();
        verify(view).hideProgress();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testNoInteractionsWhenViewReleasedOnEndOfPageReached() {
        List<Joke> listOfJokes = getListOfJokes();
        Single<List<Joke>> single = Single.just(listOfJokes);
        when(repo.getAllJokes()).thenReturn(single);
        presenter.onEndOfPageReached();
        verify(view).showProgress();
        verify(repo).getAllJokes();
        presenter.releaseView();
        testScheduler.triggerActions();
        verifyNoMoreInteractions(view, repo);
    }

    private List<Joke> getListOfJokes() {
        int count = 5;
        List<Joke> jokes = new ArrayList<>(count);
        for (int x = 0; x < count; x++) {
            jokes.add(Joke.create(x, "this is a joke", new ArrayList<>()));
        }
        return jokes;
    }
}
