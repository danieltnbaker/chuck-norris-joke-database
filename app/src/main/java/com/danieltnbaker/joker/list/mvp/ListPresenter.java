package com.danieltnbaker.joker.list.mvp;

import com.danieltnbaker.joker.model.Joke;
import com.danieltnbaker.joker.net.JokeRepo;

import java.util.List;

import io.reactivex.Scheduler;


public class ListPresenter implements ListMvp.Presenter {

    private final JokeRepo mRepo;
    private final Scheduler mSubscribeOnScheduler;
    private final Scheduler mObserveOnScheduler;
    private ListMvp.View mView;

    public ListPresenter(final JokeRepo repo,
                  final Scheduler subscribeOnScheduler,
                  final Scheduler observeOnScheduler) {
        mRepo = repo;
        mSubscribeOnScheduler = subscribeOnScheduler;
        mObserveOnScheduler = observeOnScheduler;
    }

    @Override
    public void setView(ListMvp.View view) {
        mView = view;
    }

    @Override
    public void onViewReady() {
        getJokesFromService();
    }

    @Override
    public void onEndOfPageReached() {
        getJokesFromService();
    }

    @Override
    public void releaseView() {
        mView = null;
    }

    private void getJokesFromService() {
        mView.showProgress();
        mRepo.getAllJokes()
                .subscribeOn(mSubscribeOnScheduler)
                .observeOn(mObserveOnScheduler)
                .subscribe(this::onSuccess, this::onError);
    }

    private void onSuccess(List<Joke> jokes) {
        mView.loadJokes(jokes);
        mView.hideProgress();
    }

    @SuppressWarnings("unused")
    private void onError(Throwable t) {
        mView.hideProgress();
    }
}
