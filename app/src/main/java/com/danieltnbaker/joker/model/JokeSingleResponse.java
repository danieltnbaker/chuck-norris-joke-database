package com.danieltnbaker.joker.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;


@AutoValue
public abstract class JokeSingleResponse {

    public abstract String getType();
    public abstract Joke getValue();

    public static TypeAdapter<JokeSingleResponse> typeAdapter(Gson gson) {
        return new AutoValue_JokeSingleResponse.GsonTypeAdapter(gson);
    }
}
