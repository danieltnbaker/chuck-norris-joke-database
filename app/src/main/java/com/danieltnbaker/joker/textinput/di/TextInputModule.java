package com.danieltnbaker.joker.textinput.di;


import com.danieltnbaker.joker.net.JokeRepo;
import com.danieltnbaker.joker.textinput.mvp.TextInputMvp;
import com.danieltnbaker.joker.textinput.mvp.TextInputPresenter;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Module
public class TextInputModule {

    @Provides
    TextInputMvp.Presenter providesTextInputPresenter(final JokeRepo repo) {
        return new TextInputPresenter(repo, Schedulers.io(), AndroidSchedulers.mainThread());
    }
}
