package com.danieltnbaker.joker.menu.ui;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Html;
import android.view.View;
import android.widget.Toast;

import com.danieltnbaker.joker.R;
import com.danieltnbaker.joker.di.DI;
import com.danieltnbaker.joker.list.ui.ListActivity;
import com.danieltnbaker.joker.menu.mvp.MenuMvp;
import com.danieltnbaker.joker.model.Joke;
import com.danieltnbaker.joker.textinput.ui.TextInputActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class MenuActivity extends AppCompatActivity implements MenuMvp.View {

    @BindView(R.id.checkbox_remove_explicit_jokes)
    AppCompatCheckBox checkBox;

    @Inject MenuMvp.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getBoolean(R.bool.orientation_portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        DI.getAppComponent().inject(this);
        mPresenter.setView(this);
        mPresenter.onViewReady();
    }

    @Override
    protected void onDestroy() {
        mPresenter.releaseView();
        super.onDestroy();
    }

    @OnClick({R.id.button_endless, R.id.button_random, R.id.button_search})
    public void onButtonPressed(View view) {
        switch (view.getId()) {
            case R.id.button_endless:
                mPresenter.onNeverEndingListPressed();
                break;
            case R.id.button_random:
                mPresenter.onRandomJokePressed();
                break;
            case R.id.button_search:
                mPresenter.onTextInputPressed();
                break;
        }
    }

    @OnCheckedChanged(R.id.checkbox_remove_explicit_jokes)
    public void onExplicitCheckedChanged(boolean isChecked) {
        mPresenter.onAllowExplicitJokesChecked(!isChecked);
    }

    @Override
    public void setAllowExplicitJokePreference(boolean isAllowed) {
        checkBox.setChecked(!isAllowed);
    }

    @Override
    public void showJoke(Joke joke) {
        new AlertDialog.Builder(this)
                .setMessage(Html.fromHtml(joke.getJoke()))
                .setPositiveButton(R.string.dismiss, (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    @Override
    public void launchJokeListPage() {
        startActivity(new Intent(this, ListActivity.class));
    }

    @Override
    public void launchJokeSearchPage() {
        startActivity(new Intent(this, TextInputActivity.class));
    }

    @Override
    public void showToast(@StringRes int stringResId) {
        Toast.makeText(this, stringResId, Toast.LENGTH_SHORT).show();
    }
}
