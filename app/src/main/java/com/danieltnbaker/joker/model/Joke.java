package com.danieltnbaker.joker.model;

import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.util.List;

@AutoValue
public abstract class Joke {

    public abstract int getId();
    public abstract String getJoke();
    @Nullable public abstract List<String> getCategories();

    public static Joke create(final int newId,
                              final String newJoke,
                              final List<String> newCategories) {
        return new AutoValue_Joke(newId, newJoke, newCategories);
    }

    public static TypeAdapter<Joke> typeAdapter(Gson gson) {
        return new AutoValue_Joke.GsonTypeAdapter(gson);
    }
}
