package com.danieltnbaker.joker.menu;

import com.danieltnbaker.joker.R;
import com.danieltnbaker.joker.menu.mvp.MenuMvp;
import com.danieltnbaker.joker.menu.mvp.MenuPresenter;
import com.danieltnbaker.joker.model.Joke;
import com.danieltnbaker.joker.net.JokeRepo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.TestScheduler;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MenuPresenterTests {

    @Mock
    private MenuMvp.View view;
    @Mock
    private JokeRepo repo;

    private MenuMvp.Presenter presenter;
    private TestScheduler testScheduler;

    @Before
    public void setup() {
        testScheduler = new TestScheduler();
        presenter = new MenuPresenter(repo, testScheduler, testScheduler);
        presenter.setView(view);
    }

    @After
    public void tearDown() {
        presenter.releaseView();
    }

    @Test
    public void testExplicitJokesDisabledSendsRightValueToView() {
        Single<Boolean> booleanSingle = Single.just(false);
        when(repo.areExplicitJokesAllowed()).thenReturn(booleanSingle);
        presenter.onViewReady();
        verify(repo).areExplicitJokesAllowed();
        testScheduler.triggerActions();
        verify(view).setAllowExplicitJokePreference(eq(false));
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testExplicitJokesEnabledSendsRightValueToView() {
        Single<Boolean> booleanSingle = Single.just(true);
        when(repo.areExplicitJokesAllowed()).thenReturn(booleanSingle);
        presenter.onViewReady();
        verify(repo).areExplicitJokesAllowed();
        testScheduler.triggerActions();
        verify(view).setAllowExplicitJokePreference(eq(true));
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testNetworkErrorHandledOnViewReady() {
        Single<Boolean> throwable = Single.error(new Throwable());
        when(repo.areExplicitJokesAllowed()).thenReturn(throwable);
        presenter.onViewReady();
        verify(repo).areExplicitJokesAllowed();
        testScheduler.triggerActions();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testNoInteractionsWithViewWhenReleaseViewCalled() {
        Single<Boolean> booleanSingle = Single.just(false);
        when(repo.areExplicitJokesAllowed()).thenReturn(booleanSingle);
        presenter.onViewReady();
        verify(repo).areExplicitJokesAllowed();
        presenter.releaseView();
        testScheduler.triggerActions();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testOnRandomJokePressedGetsJokeAndSendsToView() {
        Joke joke = Joke.create(1, "joke", new ArrayList<>());
        Single<Joke> jokeSingle = Single.just(joke);
        when(repo.getRandomJoke()).thenReturn(jokeSingle);
        presenter.onRandomJokePressed();
        verify(repo).getRandomJoke();
        testScheduler.triggerActions();
        verify(view).showJoke(eq(joke));
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testNetworkErrorHandledOnRandomJokePressed() {
        Single<Joke> throwable = Single.error(new Throwable());
        when(repo.getRandomJoke()).thenReturn(throwable);
        presenter.onRandomJokePressed();
        verify(repo).getRandomJoke();
        testScheduler.triggerActions();
        verify(view).showToast(ArgumentMatchers.eq(R.string.error_server_error));
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testNoViewInteractionsOnRandomJokePressedWhenViewReleased() {
        Joke joke = Joke.create(1, "joke", new ArrayList<>());
        Single<Joke> jokeSingle = Single.just(joke);
        when(repo.getRandomJoke()).thenReturn(jokeSingle);
        presenter.onRandomJokePressed();
        verify(repo).getRandomJoke();
        presenter.releaseView();
        testScheduler.triggerActions();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testJokeListPageLaunchedWhenNeverEndingPressed() {
        presenter.onNeverEndingListPressed();
        verify(view).launchJokeListPage();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testJokeSearchPageLaunchedWhenTextInputPressed() {
        presenter.onTextInputPressed();
        verify(view).launchJokeSearchPage();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testOnAllowExplicitJokesCheckedGetsJokeAndSendsToView() {
        Completable completable = Completable.complete();
        when(repo.setAllowExplicitJokesPreference(true)).thenReturn(completable);
        presenter.onAllowExplicitJokesChecked(true);
        verify(repo).setAllowExplicitJokesPreference(eq(true));
        testScheduler.triggerActions();
        verify(view).showToast(eq(R.string.toast_preference_saved));
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testNetworkErrorHandledOnAllowExplicitJokesChecked() {
        Completable completable = Completable.error(new Throwable());
        when(repo.setAllowExplicitJokesPreference(true)).thenReturn(completable);
        presenter.onAllowExplicitJokesChecked(true);
        verify(repo).setAllowExplicitJokesPreference(eq(true));
        testScheduler.triggerActions();
        verify(view).setAllowExplicitJokePreference(eq(false));
        verify(view).showToast(eq(R.string.error_check_error));
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testNoViewInteractionsOnAllowExplicitJokesCheckedWhenViewReleased() {
        Completable completable = Completable.complete();
        when(repo.setAllowExplicitJokesPreference(true)).thenReturn(completable);
        presenter.onAllowExplicitJokesChecked(true);
        verify(repo).setAllowExplicitJokesPreference(eq(true));
        presenter.releaseView();
        testScheduler.triggerActions();
        verifyNoMoreInteractions(view, repo);
    }

}