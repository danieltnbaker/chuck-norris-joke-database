package com.danieltnbaker.joker.list.ui;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.danieltnbaker.joker.R;
import com.danieltnbaker.joker.di.DI;
import com.danieltnbaker.joker.list.mvp.ListMvp;
import com.danieltnbaker.joker.model.Joke;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ListActivity extends AppCompatActivity implements ListMvp.View {

    @BindView(R.id.joke_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.progress)
    ProgressBar progressBar;

    @Inject ListMvp.Presenter mPresenter;
    private ListAdapter mAdapter;
    private AutoLoadOnScrollListener mScrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getBoolean(R.bool.orientation_portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        DI.getAppComponent().inject(this);
        mPresenter.setView(this);
        mPresenter.onViewReady();
    }

    @Override
    protected void onDestroy() {
        mScrollListener.stopListening();
        mPresenter.releaseView();
        super.onDestroy();
    }

    @Override
    public void loadJokes(List<Joke> jokes) {
        if (mAdapter == null) {
            mAdapter = new ListAdapter(jokes);
            recyclerView.setAdapter(mAdapter);
            recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
            mScrollListener = new AutoLoadOnScrollListener(mPresenter::onEndOfPageReached);
            recyclerView.addOnScrollListener(mScrollListener);
        } else {
            mAdapter.addData(jokes);
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }
}
