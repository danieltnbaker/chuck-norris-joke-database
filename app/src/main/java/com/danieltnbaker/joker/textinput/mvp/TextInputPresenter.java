package com.danieltnbaker.joker.textinput.mvp;


import android.support.annotation.VisibleForTesting;

import com.danieltnbaker.joker.R;
import com.danieltnbaker.joker.net.JokeRepo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;

public class TextInputPresenter implements TextInputMvp.Presenter {

    private static final String SPACE_DELIMITER = " ";
    private final JokeRepo mRepo;
    private final Scheduler mSubscribeOnScheduler;
    private final Scheduler mObserveOnScheduler;
    private TextInputMvp.View mView;
    private Disposable mDisposable;

    public TextInputPresenter(final JokeRepo repo,
                              final Scheduler subscribeOnScheduler,
                              final Scheduler observeOnScheduler) {
        mRepo = repo;
        mSubscribeOnScheduler = subscribeOnScheduler;
        mObserveOnScheduler = observeOnScheduler;
    }

    @Override
    public void setView(TextInputMvp.View view) {
        mView = view;
    }

    @Override
    public void onCharacterSet(String characterName) {
        if (characterName == null || characterName.isEmpty()) {
            mView.showValidationError(R.string.error_empty_character);
        } else {
            String[] names = splitCharacterNameToArray(characterName);
            mDisposable = mRepo.getJokeWithCharacter(names[0], names[1])
                    .subscribeOn(mSubscribeOnScheduler)
                    .observeOn(mObserveOnScheduler)
                    .subscribe(mView::showJoke, t -> mView.showError(t.getMessage()));
        }
    }

    @Override
    public void releaseView() {
        if (mDisposable != null && !mDisposable.isDisposed()) mDisposable.dispose();
        mView = null;
    }

    @VisibleForTesting
    public String[] splitCharacterNameToArray(String characterName) {
        List<String> nameList = new ArrayList<>(Arrays.asList(characterName.trim().split(SPACE_DELIMITER, 2)));
        if (nameList.size() < 2) {
            nameList.add(null);
        }
        return nameList.toArray(new String[0]);
    }
}
