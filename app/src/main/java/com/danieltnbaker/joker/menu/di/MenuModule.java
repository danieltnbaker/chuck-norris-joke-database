package com.danieltnbaker.joker.menu.di;

import com.danieltnbaker.joker.menu.mvp.MenuMvp;
import com.danieltnbaker.joker.menu.mvp.MenuPresenter;
import com.danieltnbaker.joker.net.JokeRepo;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Module
public class MenuModule {

    @Provides
    MenuMvp.Presenter providesMenuPresenter(final JokeRepo repo) {
        return new MenuPresenter(repo, Schedulers.io(), AndroidSchedulers.mainThread());
    }
}
