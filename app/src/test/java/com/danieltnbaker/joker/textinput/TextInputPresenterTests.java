package com.danieltnbaker.joker.textinput;


import com.danieltnbaker.joker.R;
import com.danieltnbaker.joker.model.Joke;
import com.danieltnbaker.joker.net.JokeRepo;
import com.danieltnbaker.joker.textinput.mvp.TextInputMvp;
import com.danieltnbaker.joker.textinput.mvp.TextInputPresenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import io.reactivex.Single;
import io.reactivex.schedulers.TestScheduler;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TextInputPresenterTests {

    @Mock
    private TextInputMvp.View view;
    @Mock
    private JokeRepo repo;

    private TextInputMvp.Presenter presenter;
    private TestScheduler testScheduler;

    @Before
    public void setup() {
        testScheduler = new TestScheduler();
        presenter = new TextInputPresenter(repo, testScheduler, testScheduler);
        presenter.setView(view);
    }

    @After
    public void tearDown() {
        presenter.releaseView();
    }

    @Test
    public void testGetJokeWithNullCharacterIsHandled() {
        presenter.onCharacterSet(null);
        verify(view).showValidationError(eq(R.string.error_empty_character));
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testGetJokeWithEmptyCharacterIsHandled() {
        presenter.onCharacterSet("");
        verify(view).showValidationError(eq(R.string.error_empty_character));
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testLoadingOfJokeOnCharacterSet() {
        String fullName = "Chuck Norris";
        String[] names = ((TextInputPresenter) presenter).splitCharacterNameToArray(fullName);
        Joke joke = Joke.create(1, "This is a test", new ArrayList<>());
        Single<Joke> single = Single.just(joke);
        when(repo.getJokeWithCharacter("Chuck", "Norris")).thenReturn(single);
        presenter.onCharacterSet(fullName);
        verify(repo).getJokeWithCharacter(eq(names[0]), eq(names[1]));
        testScheduler.triggerActions();
        verify(view).showJoke(eq(joke));
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testHandlesNetworkErrorOnCharacterSet() {
        String fullName = "Chuck Norris";
        String[] names = ((TextInputPresenter) presenter).splitCharacterNameToArray(fullName);
        Single<Joke> single = Single.error(new Throwable("Test error"));
        when(repo.getJokeWithCharacter("Chuck", "Norris")).thenReturn(single);
        presenter.onCharacterSet(fullName);
        verify(repo).getJokeWithCharacter(eq(names[0]), eq(names[1]));
        testScheduler.triggerActions();
        verify(view).showError(eq("Test error"));
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testNoInteractionsWhenViewReleasedOnCharacterSet() {
        String fullName = "Chuck Norris";
        String[] names = ((TextInputPresenter) presenter).splitCharacterNameToArray(fullName);
        Joke joke = Joke.create(1, "This is a test", new ArrayList<>());
        Single<Joke> single = Single.just(joke);
        when(repo.getJokeWithCharacter("Chuck", "Norris")).thenReturn(single);
        presenter.onCharacterSet(fullName);
        verify(repo).getJokeWithCharacter(eq(names[0]), eq(names[1]));
        presenter.releaseView();
        testScheduler.triggerActions();
        verifyNoMoreInteractions(view, repo);
    }

    @Test
    public void testSplitCharacterToArrayMethod() {
        String[] names = ((TextInputPresenter) presenter).splitCharacterNameToArray("Chuck");
        assertEquals(2, names.length);
        assertEquals("Chuck", names[0]);
        assertEquals(null, names[1]);
        names = ((TextInputPresenter) presenter).splitCharacterNameToArray("Chuck ");
        assertEquals(2, names.length);
        assertEquals("Chuck", names[0]);
        assertEquals(null, names[1]);
        names = ((TextInputPresenter) presenter).splitCharacterNameToArray("Chuck N");
        assertEquals(2, names.length);
        assertEquals("Chuck", names[0]);
        assertEquals("N", names[1]);
        names = ((TextInputPresenter) presenter).splitCharacterNameToArray("Chuck Norris");
        assertEquals(2, names.length);
        assertEquals("Chuck", names[0]);
        assertEquals("Norris", names[1]);
        names = ((TextInputPresenter) presenter).splitCharacterNameToArray("Chuck Awesome Norris");
        assertEquals(2, names.length);
        assertEquals("Chuck", names[0]);
        assertEquals("Awesome Norris", names[1]);
    }
}
