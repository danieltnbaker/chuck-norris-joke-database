package com.danieltnbaker.joker.menu.mvp;


import android.support.annotation.StringRes;

import com.danieltnbaker.joker.model.Joke;

public interface MenuMvp {

    interface Presenter {
        void setView(final View view);

        void onViewReady();

        void onRandomJokePressed();

        void onNeverEndingListPressed();

        void onTextInputPressed();

        void onAllowExplicitJokesChecked(boolean allow);

        void releaseView();
    }

    interface View {
        void setAllowExplicitJokePreference(boolean allow);

        void showJoke(final Joke joke);

        void launchJokeListPage();

        void launchJokeSearchPage();

        void showToast(@StringRes int stringResId);
    }
}
