package com.danieltnbaker.joker.net;


import com.danieltnbaker.joker.model.Joke;
import com.danieltnbaker.joker.model.JokeMultipleResponse;
import com.danieltnbaker.joker.model.JokeSingleResponse;
import com.danieltnbaker.joker.utils.PreferenceHelper;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public class JokeRepo {

    private static final String EXCLUDE_EXPLICIT = "explicit";
    private final JokeService mService;
    private final PreferenceHelper mPreferenceHelper;

    public JokeRepo(final JokeService service,
                    final PreferenceHelper preferenceHelper) {
        mService = service;
        mPreferenceHelper = preferenceHelper;
    }

    public Single<List<Joke>> getAllJokes() {
        return mService.getAllJokes(getExplicitValue())
                .map(JokeMultipleResponse::getValue);
    }

    public Single<Joke> getJokeWithCharacter(String firstName, String lastName) {
        return mService.getJokeWithCharacter(firstName, lastName, getExplicitValue())
                .map(JokeSingleResponse::getValue);
    }

    public Single<Joke> getRandomJoke() {
        return mService.getRandomJoke(getExplicitValue())
                .map(JokeSingleResponse::getValue);
    }

    public Completable setAllowExplicitJokesPreference(boolean allow) {
        return Completable.create(e -> {
            if (mPreferenceHelper.getAllowExplicitJokesPreference() != allow) {
                mPreferenceHelper.setAllowExplicitJokesPreference(allow);
                e.onComplete();
            }
        });
    }

    public Single<Boolean> areExplicitJokesAllowed() {
        return Single.create(e -> e.onSuccess(mPreferenceHelper.getAllowExplicitJokesPreference()));
    }

    private String getExplicitValue() {
        if (mPreferenceHelper.getAllowExplicitJokesPreference()) {
            return EXCLUDE_EXPLICIT;
        }
        return null;
    }

}
