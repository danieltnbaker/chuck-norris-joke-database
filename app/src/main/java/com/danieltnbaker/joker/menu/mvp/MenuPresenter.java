package com.danieltnbaker.joker.menu.mvp;


import com.danieltnbaker.joker.R;
import com.danieltnbaker.joker.net.JokeRepo;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;

public class MenuPresenter implements MenuMvp.Presenter {

    private final JokeRepo mRepo;
    private final Scheduler mSubscribeOnScheduler;
    private final Scheduler mObserveOnScheduler;
    private MenuMvp.View mView;
    private CompositeDisposable mDisposable;

    public MenuPresenter(JokeRepo repo,
                         Scheduler subscribeOnScheduler,
                         Scheduler observeOnScheduler) {
        mRepo = repo;
        mSubscribeOnScheduler = subscribeOnScheduler;
        mObserveOnScheduler = observeOnScheduler;
        mDisposable = new CompositeDisposable();
    }

    @Override
    public void setView(MenuMvp.View view) {
        mView = view;
    }

    @Override
    public void onViewReady() {
        mDisposable.add(mRepo.areExplicitJokesAllowed()
                .subscribeOn(mSubscribeOnScheduler)
                .observeOn(mObserveOnScheduler)
                .subscribe(mView::setAllowExplicitJokePreference));
    }

    @Override
    public void onRandomJokePressed() {
        mDisposable.add(mRepo.getRandomJoke()
                .subscribeOn(mSubscribeOnScheduler)
                .observeOn(mObserveOnScheduler)
                .subscribe(mView::showJoke, t -> mView.showToast(R.string.error_server_error)));
    }

    @Override
    public void onNeverEndingListPressed() {
        mView.launchJokeListPage();
    }

    @Override
    public void onTextInputPressed() {
        mView.launchJokeSearchPage();
    }

    @Override
    public void onAllowExplicitJokesChecked(boolean allow) {
        mDisposable.add(mRepo.setAllowExplicitJokesPreference(allow)
                .subscribeOn(mSubscribeOnScheduler)
                .observeOn(mObserveOnScheduler)
                .subscribe(() -> mView.showToast(R.string.toast_preference_saved),
                        t -> {
                            mView.setAllowExplicitJokePreference(!allow);
                            mView.showToast(R.string.error_check_error);
                        }));
    }

    @Override
    public void releaseView() {
        if (mDisposable != null && !mDisposable.isDisposed()) mDisposable.dispose();
        mView = null;
    }

}
