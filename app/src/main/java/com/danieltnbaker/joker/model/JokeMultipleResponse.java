package com.danieltnbaker.joker.model;


import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;

import java.util.List;

@AutoValue
public abstract class JokeMultipleResponse {

    public abstract String getType();
    public abstract List<Joke> getValue();

    public static TypeAdapter<JokeMultipleResponse> typeAdapter(Gson gson) {
        return new AutoValue_JokeMultipleResponse.GsonTypeAdapter(gson);
    }
}
