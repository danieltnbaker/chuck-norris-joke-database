package com.danieltnbaker.joker.di;

import android.content.Context;

import com.danieltnbaker.joker.JokerApplication;
import com.danieltnbaker.joker.utils.PreferenceHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    Context provideContext() {
        return JokerApplication.get();
    }

    @Provides
    @Singleton
    PreferenceHelper providePreferenceHelper(Context context) {
        return new PreferenceHelper(context);
    }
}
