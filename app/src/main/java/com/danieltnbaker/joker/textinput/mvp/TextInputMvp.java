package com.danieltnbaker.joker.textinput.mvp;


import android.support.annotation.StringRes;

import com.danieltnbaker.joker.model.Joke;

public interface TextInputMvp {

    interface Presenter {
        void setView(View view);

        void onCharacterSet(String characterName);

        void releaseView();
    }

    interface View {
        void showJoke(final Joke joke);

        void showValidationError(@StringRes int resId);

        void showError(final String message);
    }
}
