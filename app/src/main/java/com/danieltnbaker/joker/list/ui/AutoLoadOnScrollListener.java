package com.danieltnbaker.joker.list.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class AutoLoadOnScrollListener extends RecyclerView.OnScrollListener {

    private static final int THRESHOLD = 8;
    private volatile boolean mIsLoading;
    private int mPreviousTotal;

    private final Callback mCallback;
    private CompositeDisposable mDisposable;

    AutoLoadOnScrollListener(@NonNull final Callback callback) {
        mCallback = callback;
        mDisposable = new CompositeDisposable();
        mIsLoading = true;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        mDisposable.add(Completable.create(completableEmitter -> {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            final int visibleItemCount = recyclerView.getChildCount();
            final int totalItemCount = layoutManager.getItemCount();
            final int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

            if (mIsLoading) {
                if (totalItemCount > mPreviousTotal) {
                    mIsLoading = false;
                    mPreviousTotal = totalItemCount;
                }
            }
            if (!mIsLoading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + THRESHOLD)) {
                completableEmitter.onComplete();
                mIsLoading = true;
            }
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mCallback::onLoadMoreNeeded));
    }

    public void stopListening() {
        if (mDisposable != null && !mDisposable.isDisposed()) mDisposable.dispose();
    }

    public interface Callback {
        void onLoadMoreNeeded();
    }
}
